USE FIFA2010
GO

EXEC PlayTournament
GO

-- ���������� �����, ������� �� ����� �������
SELECT COUNT(Id)
FROM Goals
GO

-- ������� ���������� ����� � �����
SELECT AVG(TC.C)
FROM (SELECT COUNT(Id) AS C
FROM Goals
GROUP BY MatchFK) TC
GO

-- ���������� ���������� ������� ����� �������� 
SELECT C.Name AS Country, COUNT(G.Id) AS CountOfGoals
FROM Teams T, Goals G, Countries C, Players P
WHERE T.Id = P.TeamFK AND P.Id = G.PlayerScoredGoalFK AND C.Id = T.CountryFK
GROUP BY C.Name
HAVING COUNT(G.Id) IN 
(SELECT MAX(TC.C)
FROM (SELECT COUNT(G.Id) AS C
FROM Goals G, Teams T, Players P
WHERE G.PlayerScoredGoalFK = P.Id AND P.TeamFK = T.Id
GROUP BY T.Id) TC)
GO

-- ���������� ���������� ������� ����� �������� 
SELECT C.Name AS Country, COUNT(G.Id) AS CountOfGoals
FROM Teams T, Goals G, Countries C, Players P
WHERE T.Id = P.TeamFK AND P.Id = G.PlayerScoredGoalFK AND C.Id = T.CountryFK
GROUP BY C.Name
HAVING COUNT(G.Id) IN 
(SELECT MIN(TC.C)
FROM (SELECT COUNT(G.Id) AS C
FROM Goals G, Teams T, Players P
WHERE G.PlayerScoredGoalFK = P.Id AND P.TeamFK = T.Id
GROUP BY T.Id) TC)
GO

-- ���������� ���������� ����������� ����� ��������
SELECT C.Name AS Country, COUNT(G.Id) AS CountOfGoals
FROM Teams T, Goals G, Countries C
WHERE T.Id = G.TeamMissedGoalFK AND C.Id = T.CountryFK
GROUP BY C.Name
HAVING COUNT(G.Id) IN 
(SELECT MAX(TC.C)
FROM (SELECT COUNT(Id) AS C
FROM Goals
GROUP BY TeamMissedGoalFK) TC)
GO

-- ���������� ���������� ����������� ����� �������� 
SELECT C.Name AS Country, COUNT(G.Id) AS CountOfGoals
FROM Teams T, Goals G, Countries C
WHERE T.Id = G.TeamMissedGoalFK AND C.Id = T.CountryFK
GROUP BY C.Name
HAVING COUNT(G.Id) IN
(SELECT MIN(TC.C)
FROM (SELECT COUNT(Id) AS C
FROM Goals
GROUP BY TeamMissedGoalFK) TC)
GO

-- ��������� ������������ ���� ������
SELECT SUM(CountOfSpectators) AS SumOfSpectators
FROM MatchResults
GO

-- ������� ������������ ������ �����
SELECT AVG(CountOfSpectators) AS AverageSpectators
FROM MatchResults
GO

-- ������� � ���������� ����������� �����
SELECT C.Name
FROM Teams T, Countries C
WHERE Victories IN (SELECT MAX(Victories) FROM Teams)
AND C.Id = T.CountryFK
GO

-- ������� � ���������� ����������� �����
SELECT C.Name
FROM Teams T, Countries C
WHERE Victories IN (SELECT MIN(Victories) FROM Teams)
AND C.Id = T.CountryFK
GO

-- ������� � ���������� ����������� ���������
SELECT C.Name
FROM Teams T, Countries C
WHERE Loses IN (SELECT MAX(Loses) FROM Teams)
AND C.Id = T.CountryFK
GO

-- ������� � ���������� ����������� ���������
SELECT C.Name
FROM Teams T, Countries C
WHERE Loses IN (SELECT MIN(Loses) FROM Teams)
AND C.Id = T.CountryFK
GO

-- ������ ����������
SELECT C.Name AS Country, P.Name AS Player, COUNT(G.Id) AS CountOfGoals
FROM Goals G, Players P, Teams T, Countries C
WHERE P.Id = G.PlayerScoredGoalFK AND P.TeamFK = T.Id AND C.Id = T.CountryFK
GROUP BY P.Name, C.Name
HAVING COUNT(G.Id) IN 
(SELECT MAX(TC.C)
FROM (SELECT COUNT(Id) AS C
FROM Goals
GROUP BY PlayerScoredGoalFK) TC)
GO

-- ������ ������, �������� �������� �����
SELECT C.Name
FROM Countries C, Teams T, PlacesInTournament P
WHERE C.Id = T.CountryFK AND T.Id = P.TeamFk AND (P.Place = '1' OR P.Place = '2' OR P.Place = '3')
ORDER BY Place
GO