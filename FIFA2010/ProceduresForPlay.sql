USE FIFA2010
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'RefreshTournament' AND type = 'P')
    DROP PROCEDURE RefreshTournament;
GO

CREATE PROCEDURE RefreshTournament
AS
BEGIN

	IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Goals' AND type = 'U')
	    DROP TABLE Goals;
	
	IF EXISTS (SELECT * FROM sys.objects WHERE name = 'MatchResults' AND type = 'U')
	    DROP TABLE MatchResults;

	IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PlacesInTournament' AND type = 'U')
	    DROP TABLE PlacesInTournament;
	
	IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Groups' AND type = 'U')
	    DROP TABLE Groups;
	
	IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GroupResults' AND type = 'U')
	    DROP TABLE GroupResults;
	
	IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Matches' AND type = 'U')
	    DROP TABLE Matches;
	
	CREATE TABLE Matches
	(
		Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
		[Date] date,
		[Time] time,
		Team1 bigint,
		Team2 bigint,
		StadiumFK bigint
	
		FOREIGN KEY (StadiumFK) REFERENCES Stadiums(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		FOREIGN KEY (Team1) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		FOREIGN KEY (Team2) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
	)
	
	CREATE TABLE MatchResults
	(
		Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
		CountOfSpectators int,
		Winner bigint,
		Loser bigint,
		MatchFK bigint
	
		FOREIGN KEY (MatchFK) REFERENCES Matches(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		FOREIGN KEY (Winner) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		FOREIGN KEY (Loser) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
	)
	
	CREATE TABLE Goals
	(
		Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
		[Time] time,
		PlayerScoredGoalFK bigint,
		TeamMissedGoalFK bigint,
		MatchFK bigint
	
		FOREIGN KEY (PlayerScoredGoalFK) REFERENCES Players(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		FOREIGN KEY (TeamMissedGoalFK) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		FOREIGN KEY (MatchFK) REFERENCES Matches(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
	)
	
	CREATE TABLE Groups
	(
		Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
		Team1 bigint,
		Team2 bigint,
		Team3 bigint,
		Team4 bigint
	
		FOREIGN KEY (Team1) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		FOREIGN KEY (Team2) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		FOREIGN KEY (Team3) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		FOREIGN KEY (Team4) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
	)
	
	CREATE TABLE GroupResults
	(
		Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
		FirstPlace bigint,
		SecondPlace bigint
		
		FOREIGN KEY (FirstPlace) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
		FOREIGN KEY (SecondPlace) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
	)
	
	CREATE TABLE PlacesInTournament
	(
		Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
		Place nvarchar(5),
		TeamFK bigint
		
		FOREIGN KEY (TeamFK) REFERENCES Teams(Id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
	)
END
GO

EXEC RefreshTournament
GO

-- �������� ����
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'MakeGoal' AND type = 'P')
    DROP PROCEDURE MakeGoal;
GO

CREATE PROCEDURE MakeGoal
	@teamMadeId bigint,
	@teamMissedId bigint,
	@matchId bigint
AS
BEGIN
	DECLARE @player bigint

	DECLARE @time time
	DECLARE @minutes int
	DECLARE @seconds int
	DECLARE @hours int

	-- �������� ���������� ������ �� �������
	SET @player = (SELECT TOP(1) P.Id FROM Players P WHERE P.TeamFK = @teamMadeId ORDER BY NEWID())

	-- ��������� ����� ����
	SET @seconds = (SELECT ABS(CHECKSUM(NEWID()) % 60))
	SET @minutes = (SELECT ABS(CHECKSUM(NEWID()) % 90))
	SET @hours = @minutes / 60
	SET @minutes = @minutes % 60
	SET @time = CAST(@hours AS char) + ':' + CAST(@minutes AS char) + ':' + CAST(@seconds AS char)

	INSERT INTO Goals ([Time], PlayerScoredGoalFK, TeamMissedGoalFK, MatchFK) VALUES (@time, @player, @teamMissedId, @matchId)
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PlayMatch' AND type = 'P')
    DROP PROCEDURE PlayMatch;
GO

CREATE PROCEDURE PlayMatch
	@matchId bigint
AS
BEGIN
	DECLARE @team1 bigint
	DECLARE @team2 bigint
	SET @team1 = (SELECT T.Id FROM Teams T, Matches M WHERE T.Id = M.Team1 AND M.Id = @matchId)
	SET @team2 = (SELECT T.Id FROM Teams T, Matches M WHERE T.Id = M.Team2 AND M.Id = @matchId)

	-- "���������" ��������
	DECLARE @countOfSpectators int
	DECLARE @stadiumId bigint
	DECLARE @stadiumCapacity int
	SET @stadiumId = (SELECT ABS(CHECKSUM(NEWID()) % 12) + 1)
	SET @stadiumCapacity = (SELECT Capacity FROM Stadiums WHERE Id = @stadiumId)
	SET @countOfSpectators = (SELECT ABS(CHECKSUM(NEWID()) % @stadiumCapacity) + 1)

	-- ���������� ���������� ����� 
	DECLARE @countOfGoals int
	SET @countOfGoals = (SELECT ABS(CHECKSUM(NEWID()) % 10) + 1)
	IF @countOfGoals % 2 = 0
	BEGIN
		SET @countOfGoals = @countOfGoals + 1
	END
	-- �������� ����
	DECLARE @i int
	SET @i = 0
	WHILE @i < @countOfGoals
	BEGIN
		IF (SELECT ABS(CHECKSUM(NEWID()) % 2)) = 0
		BEGIN
			EXEC MakeGoal @team1, @team2, @matchId
		END
		ELSE
		BEGIN
			EXEC MakeGoal @team2, @team1, @matchId
		END		
		SET @i = @i + 1
	END
	
	-- ��������� ����������
	DECLARE @winner int
	SET @winner = (
	SELECT TOP(1) T.Id
	FROM (SELECT SUM(G.Id) AS S, T.Id
	FROM Goals G, Teams T
	WHERE G.TeamMissedGoalFK = T.Id AND T.Id IN
	(
		SELECT T.Id
		FROM Teams T, Matches M
		WHERE M.Id = @matchId AND (T.Id = M.Team1 OR T.Id = M.Team2)
	)
	GROUP BY T.Id) AS T
	ORDER BY T.S)

	-- ��������� ������������
	DECLARE @loser int
	SET @loser = (SELECT T.Id FROM Teams T, Matches M WHERE (T.Id = M.Team1 OR T.Id = M.Team2) AND M.Id = @matchId AND T.Id <> @winner)	

	INSERT INTO MatchResults (CountOfSpectators, Winner, Loser, MatchFK) VALUES (@countOfSpectators, @winner, @loser, @matchId)
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GetRandomMatchTime' AND type = 'P')
    DROP PROCEDURE GetRandomMatchTime;
GO

CREATE PROCEDURE GetRandomMatchTime
	@time time OUTPUT
AS
BEGIN
	DECLARE @hours int
	DECLARE @minutes int

	SET @hours = (SELECT ABS(CHECKSUM(NEWID()) % 9) + 9)
	SET @minutes = (SELECT ABS(CHECKSUM(NEWID()) % 4))
	SET @minutes = @minutes * 15

	SET @time = CAST(@hours AS char) + ':' + CAST(@minutes AS char) + ':00'
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GetRandomStadium' AND type = 'P')
    DROP PROCEDURE GetRandomStadium;
GO

CREATE PROCEDURE GetRandomStadium
	@stadium bigint OUTPUT
AS
BEGIN
	SET @stadium = (SELECT ABS(CHECKSUM(NEWID()) % (SELECT COUNT(*) FROM Stadiums S)) + 1)
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'UpdateVictoriesAndLoses' AND type = 'P')
    DROP PROCEDURE UpdateVictoriesAndLoses;
GO

CREATE PROCEDURE UpdateVictoriesAndLoses
	@matchId bigint
AS
BEGIN
	DECLARE @winner bigint
	DECLARE @loser bigint

	SET @winner = (SELECT TOP(1) Winner FROM MatchResults WHERE MatchFK = @matchId)
	SET @loser = (SELECT TOP(1) Loser FROM MatchResults WHERE MatchFK = @matchId)

	UPDATE Teams
	SET Victories = Victories + 1
	WHERE Id = @winner
	UPDATE Teams
	SET Loses = Loses + 1
	WHERE Id = @loser
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'FillGroups' AND type = 'P')
    DROP PROCEDURE FillGroups;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GetTeamsWithoutGroups' AND type = 'V')
    DROP VIEW GetTeamsWithoutGroups;
GO

CREATE VIEW GetTeamsWithoutGroups
AS
SELECT T.Id 
FROM Teams T
WHERE T.Id NOT IN (SELECT T.Id FROM Teams T,  Groups G WHERE T.Id = G.Team1 OR T.Id = G.Team2 OR T.Id = G.Team3 OR T.Id = G.Team4)
GO

CREATE PROCEDURE FillGroups
AS
BEGIN	
	DECLARE @team1 bigint
	DECLARE @team2 bigint
	DECLARE @team3 bigint
	DECLARE @team4 bigint

	SET @team1 = (SELECT TOP(1) Id FROM Teams ORDER BY NEWID())
	SET @team2 = (SELECT TOP(1) Id FROM Teams WHERE Id <> @team1 ORDER BY NEWID())
	SET @team3 = (SELECT TOP(1) Id FROM Teams WHERE Id <> @team1 AND Id <> @team2 ORDER BY NEWID())
	SET @team4 = (SELECT TOP(1) Id FROM Teams WHERE Id <> @team1 AND Id <> @team2 AND Id <> @team3 ORDER BY NEWID())

	INSERT INTO Groups (Team1, Team2, Team3, Team4) VALUES (@team1, @team2, @team3, @team4)

	DECLARE @i int
	SET @i = 2

	WHILE @i <= 8
	BEGIN		
		SET @team1 = (SELECT TOP(1) Id FROM GetTeamsWithoutGroups ORDER BY NEWID())
		SET @team2 = (SELECT TOP(1) Id FROM GetTeamsWithoutGroups WHERE Id <> @team1 ORDER BY NEWID())
		SET @team3 = (SELECT TOP(1) Id FROM GetTeamsWithoutGroups WHERE Id <> @team1 AND Id <> @team2 ORDER BY NEWID())
		SET @team4 = (SELECT TOP(1) Id FROM GetTeamsWithoutGroups WHERE Id <> @team1 AND Id <> @team2 AND Id <> @team3 ORDER BY NEWID())

		INSERT INTO Groups (Team1, Team2, Team3, Team4) VALUES (@team1, @team2, @team3, @team4)
		SET @i = @i + 1
	END
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'CreateMatchAndPlayIt' AND type = 'P')
    DROP PROCEDURE CreateMatchAndPlayIt;
GO

CREATE PROCEDURE CreateMatchAndPlayIt
	@date date,
	@team1 bigint,
	@team2 bigint
AS
BEGIN
	DECLARE @time time
	DECLARE @stadium bigint

	DECLARE @matchId bigint
	DECLARE @matchResultId bigint
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES (@date, @time, @stadium, @team1, @team2)
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PlayFinal' AND type = 'P')
    DROP PROCEDURE PlayFinal;
GO

CREATE PROCEDURE PlayFinal
AS
BEGIN	
	DECLARE @time time
	DECLARE @stadium bigint

	DECLARE @matchId bigint
	DECLARE @matchResultId bigint
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-17-2010', @time, @stadium, (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 2 FROM MatchResults)), (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 1 FROM MatchResults)))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'2')
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'1')
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PlayMatchForThirdPlace' AND type = 'P')
    DROP PROCEDURE PlayMatchForThirdPlace;
GO

CREATE PROCEDURE PlayMatchForThirdPlace
AS
BEGIN	
	DECLARE @time time
	DECLARE @stadium bigint

	DECLARE @matchId bigint
	DECLARE @matchResultId bigint
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-16-2010', @time, @stadium, (SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) - 1 FROM MatchResults)), (SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'4')
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'3')

	EXEC PlayFinal
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PlaySemiFinals' AND type = 'P')
    DROP PROCEDURE PlaySemiFinals;
GO

CREATE PROCEDURE PlaySemiFinals
AS
BEGIN	
	DECLARE @time time
	DECLARE @stadium bigint

	DECLARE @matchId bigint
	DECLARE @matchResultId bigint

	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-14-2010', @time, @stadium, (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 3 FROM MatchResults)), (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 2 FROM MatchResults)))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-15-2010', @time, @stadium, (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 2 FROM MatchResults)), (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 1 FROM MatchResults)))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId

	EXEC PlayMatchForThirdPlace
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PlaySecondRoundOfPlayOff' AND type = 'P')
    DROP PROCEDURE PlaySecondRoundOfPlayOff;
GO

CREATE PROCEDURE PlaySecondRoundOfPlayOff
AS
BEGIN	
	DECLARE @time time
	DECLARE @stadium bigint

	DECLARE @matchId bigint
	DECLARE @matchResultId bigint

	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-12-2010', @time, @stadium, (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 7 FROM MatchResults)), (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 6 FROM MatchResults)))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'5-8')
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-12-2010', @time, @stadium, (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 6 FROM MatchResults)), (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 5 FROM MatchResults)))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'5-8')
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-13-2010', @time, @stadium, (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 5 FROM MatchResults)), (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 4 FROM MatchResults)))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'5-8')
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-13-2010', @time, @stadium, (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 4 FROM MatchResults)), (SELECT Winner FROM MatchResults WHERE Id = (SELECT MAX(Id) - 3 FROM MatchResults)))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'5-8')	
	
	EXEC PlaySemiFinals
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PlayFirstRoundOfPlayOff' AND type = 'P')
    DROP PROCEDURE PlayFirstRoundOfPlayOff;
GO

CREATE PROCEDURE PlayFirstRoundOfPlayOff
AS
BEGIN	
	DECLARE @time time
	DECLARE @stadium bigint

	DECLARE @matchId bigint
	DECLARE @matchResultId bigint

	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-08-2010', @time, @stadium, (SELECT FirstPlace FROM GroupResults WHERE Id = 1), (SELECT SecondPlace FROM GroupResults WHERE Id = 2))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'9-16')
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-08-2010', @time, @stadium, (SELECT FirstPlace FROM GroupResults WHERE Id = 2), (SELECT SecondPlace FROM GroupResults WHERE Id = 1))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'9-16')
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-09-2010', @time, @stadium, (SELECT FirstPlace FROM GroupResults WHERE Id = 3), (SELECT SecondPlace FROM GroupResults WHERE Id = 4))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'9-16')
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-09-2010', @time, @stadium, (SELECT FirstPlace FROM GroupResults WHERE Id = 4), (SELECT SecondPlace FROM GroupResults WHERE Id = 3))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'9-16')
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-10-2010', @time, @stadium, (SELECT FirstPlace FROM GroupResults WHERE Id = 5), (SELECT SecondPlace FROM GroupResults WHERE Id = 6))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'9-16')
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-10-2010', @time, @stadium, (SELECT FirstPlace FROM GroupResults WHERE Id = 6), (SELECT SecondPlace FROM GroupResults WHERE Id = 5))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'9-16')
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-11-2010', @time, @stadium, (SELECT FirstPlace FROM GroupResults WHERE Id = 7), (SELECT SecondPlace FROM GroupResults WHERE Id = 8))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'9-16')
	
	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-11-2010', @time, @stadium, (SELECT FirstPlace FROM GroupResults WHERE Id = 8), (SELECT SecondPlace FROM GroupResults WHERE Id = 7))
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	INSERT INTO PlacesInTournament (TeamFK, Place) VALUES ((SELECT Loser FROM MatchResults WHERE Id = (SELECT MAX(Id) FROM MatchResults)), N'9-16')

	EXEC PlaySecondRoundOfPlayOff
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PlayGroup' AND type = 'P')
    DROP PROCEDURE PlayGroup;
GO

CREATE PROCEDURE PlayGroup
	@group bigint
AS
BEGIN
	EXEC FillGroups

	DECLARE @team1 bigint
	DECLARE @team2 bigint
	DECLARE @team3 bigint
	DECLARE @team4 bigint

	DECLARE @time time
	DECLARE @stadium bigint

	DECLARE @matchId bigint
	DECLARE @matchResultId bigint

	SET @team1 = (SELECT T.Id FROM Teams T, Groups G WHERE G.Team1 = T.Id AND G.Id = @group)
	SET @team2 = (SELECT T.Id FROM Teams T, Groups G WHERE G.Team2 = T.Id AND G.Id = @group)
	SET @team3 = (SELECT T.Id FROM Teams T, Groups G WHERE G.Team3 = T.Id AND G.Id = @group)
	SET @team4 = (SELECT T.Id FROM Teams T, Groups G WHERE G.Team4 = T.Id AND G.Id = @group)

	UPDATE Teams
	SET Victories = 0, Loses = 0
	WHERE Id = @team1 OR Id = @team2 OR Id = @team3 OR Id = @team4

	EXEC GetRandomMatchTime @time OUTPUT 	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-01-2010', @time, @stadium, @team1, @team2)
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId

	EXEC GetRandomMatchTime @time OUTPUT	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-02-2010', @time, @stadium, @team1, @team3)
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId

	EXEC GetRandomMatchTime @time OUTPUT	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-03-2010', @time, @stadium, @team1, @team4)
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	
	EXEC GetRandomMatchTime @time OUTPUT	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-04-2010', @time, @stadium, @team2, @team3)
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	
	EXEC GetRandomMatchTime @time OUTPUT	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-05-2010', @time, @stadium, @team2, @team4)
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	
	EXEC GetRandomMatchTime @time OUTPUT	
	EXEC GetRandomStadium @stadium OUTPUT
	INSERT INTO Matches ([Date], [Time], StadiumFK, Team1, Team2) VALUES ('06-06-2010', @time, @stadium, @team3, @team4)
	SET @matchId = (SELECT MAX(Id) FROM Matches)
	EXEC PlayMatch @matchId
	EXEC UpdateVictoriesAndLoses @matchId
	
	DECLARE @firstPlace bigint
	DECLARE @secondPlace bigint
	DECLARE @thirdPlace bigint
	DECLARE @fourthPlace bigint

	SET @firstPlace = (SELECT Id FROM (SELECT TOP(1) Id FROM Teams T WHERE T.Id = @team1 OR  T.Id = @team2 OR  T.Id = @team3 OR  T.Id = @team4 ORDER BY  T.Victories DESC) T)
	SET @secondPlace = (SELECT Id FROM (SELECT TOP(1) Id FROM Teams T WHERE (T.Id = @team1 OR  T.Id = @team2 OR  T.Id = @team3 OR  T.Id = @team4) AND T.Id <> @firstPlace ORDER BY  T.Victories DESC) T)
	SET @thirdPlace = (SELECT Id FROM (SELECT TOP(1) Id FROM Teams T WHERE (T.Id = @team1 OR  T.Id = @team2 OR  T.Id = @team3 OR  T.Id = @team4) AND T.Id <> @firstPlace AND T.Id <> @secondPlace ORDER BY  T.Victories DESC) T)
	SET @fourthPlace = (SELECT Id FROM (SELECT TOP(1) Id FROM Teams T WHERE (T.Id = @team1 OR  T.Id = @team2 OR  T.Id = @team3 OR  T.Id = @team4) AND T.Id <> @firstPlace AND T.Id <> @secondPlace AND T.Id <> @thirdPlace ORDER BY  T.Victories DESC) T)

	IF (SELECT Victories FROM Teams WHERE Id = @firstPlace) = (SELECT Victories FROM Teams WHERE Id = @secondPlace) OR
	(SELECT Victories FROM Teams WHERE Id = @thirdPlace) = (SELECT Victories FROM Teams WHERE Id = @secondPlace)
	BEGIN
		EXEC PlayGroup @group
	END
	ELSE
	BEGIN
		INSERT INTO GroupResults (FirstPlace, SecondPlace) VALUES (@firstPlace, @secondPlace)
		INSERT INTO PlacesInTournament (TeamFK, Place) VALUES (@thirdPlace, N'17-32')
		INSERT INTO PlacesInTournament (TeamFK, Place) VALUES (@fourthPlace, N'17-32')
	END
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PlayAllGroups' AND type = 'P')
    DROP PROCEDURE PlayAllGroups;
GO

CREATE PROCEDURE PlayAllGroups
AS
BEGIN	
	DECLARE @i int
	SET @i = 1

	WHILE @i <= 8
	BEGIN
		EXEC PlayGroup @i
		SET @i = @i + 1
	END
	EXEC PlayFirstRoundOfPlayOff
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PlayTournament' AND type = 'P')
    DROP PROCEDURE PlayTournament;
GO

CREATE PROCEDURE PlayTournament
AS
BEGIN

EXEC RefreshTournament
EXEC PlayAllGroups

SELECT C.Name
FROM Countries C, Teams T, PlacesInTournament P
WHERE C.Id = T.CountryFK AND T.Id = P.TeamFk AND (P.Place = '1' OR P.Place = '2' OR P.Place = '3')
ORDER BY Place
END
GO