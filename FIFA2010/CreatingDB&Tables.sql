IF DB_ID('FIFA2010') IS NOT NULL
BEGIN
	USE master
    ALTER DATABASE FIFA2010 SET single_user with rollback immediate
    DROP DATABASE FIFA2010
END
GO

CREATE DATABASE FIFA2010
GO

USE FIFA2010
GO

CREATE TABLE Countries
(
	Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(50)
)
GO

CREATE TABLE Equipment
(
	Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(50)
)
GO

CREATE TABLE Teams
(
	Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
	CountryFK bigint,
	EquipmentFK bigint,
	Victories int NOT NULL,
	Loses int NOT NULL

	FOREIGN KEY (CountryFK) REFERENCES Countries(Id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
	FOREIGN KEY (EquipmentFK) REFERENCES Equipment(Id)
			ON DELETE CASCADE
			ON UPDATE CASCADE
)
GO

CREATE TABLE Trainers
(
	Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(50),
	TeamFK bigint

	FOREIGN KEY (TeamFK) REFERENCES Teams(Id)
			ON DELETE CASCADE
			ON UPDATE CASCADE
)
GO

CREATE TABLE Players
(
	Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(50),
	Position nvarchar(2),
	TeamFK bigint

	FOREIGN KEY (TeamFK) REFERENCES Teams(Id)
			ON DELETE CASCADE
			ON UPDATE CASCADE
)
GO

CREATE TABLE FootballAssociations
(
	Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(50)
)
GO

CREATE TABLE Referees
(
	Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(50),
	CountryFK bigint,
	FootballAssociationFK bigint
	
	FOREIGN KEY (CountryFK) REFERENCES Countries(Id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
	FOREIGN KEY (FootballAssociationFK) REFERENCES FootballAssociations(Id)
			ON DELETE CASCADE
			ON UPDATE CASCADE
)
GO

CREATE TABLE Cities
(
	Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(50)
)
GO

CREATE TABLE Stadiums
(
	Id bigint NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(50),
	Capacity int,
	CityFK bigint

	FOREIGN KEY (CityFK) REFERENCES Cities(Id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
)
GO

USE FIFA2010
GO

-- ������
CREATE TABLE PlayersTemp
(
    Position nvarchar(2),
    Name nvarchar(50)
)
GO

BULK INSERT PlayersTemp
FROM 'D:\Programming\Projects\SQL\fifa2010\FIFA2010\������.txt'
WITH (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n')
GO

INSERT INTO Players (Position, Name)
SELECT Position, Name
FROM PlayersTemp
GO

DROP TABLE PlayersTemp
GO

-- �������� + ������
CREATE TABLE StadiumsTemp
(
    City nvarchar(50),
    Name nvarchar(50),
	Capacity nvarchar(50)
)
GO

BULK INSERT StadiumsTemp
FROM 'D:\Programming\Projects\SQL\fifa2010\FIFA2010\��������.txt'
WITH (FIELDTERMINATOR = ',', ROWTERMINATOR = '\n')
GO

INSERT INTO Cities (Name)
SELECT City
FROM StadiumsTemp
GROUP BY City
GO

INSERT INTO Stadiums (CityFK, Name, Capacity)
SELECT Cities.Id, StadiumsTemp.Name, CAST(Capacity AS int)
FROM StadiumsTemp, Cities
WHERE Cities.Name = StadiumsTemp.City
GO

DROP TABLE StadiumsTemp
GO

-- ������
CREATE TABLE CountriesTemp
(
    Name nvarchar(50)
)
GO

BULK INSERT CountriesTemp
FROM 'D:\Programming\Projects\SQL\fifa2010\FIFA2010\������.txt'
WITH (ROWTERMINATOR = '\n')
GO

INSERT INTO Countries (Name)
SELECT Name
FROM CountriesTemp
GO

DROP TABLE CountriesTemp
GO

-- ����� + ����������
CREATE TABLE RefereesTemp
(
    Association nvarchar(50),
	Country nvarchar(50),
    Name nvarchar(50)
)
GO

BULK INSERT RefereesTemp
FROM 'D:\Programming\Projects\SQL\fifa2010\FIFA2010\�����.txt'
WITH (FIELDTERMINATOR = ',', ROWTERMINATOR = '\n')
GO

INSERT INTO FootballAssociations (Name)
SELECT Association
FROM RefereesTemp
GROUP BY Association
GO

INSERT INTO Referees (Name, FootballAssociationFK, CountryFK)
SELECT RefereesTemp.Name, FootballAssociations.Id, Countries.Id
FROM RefereesTemp, FootballAssociations, Countries
WHERE FootballAssociations.Name = RefereesTemp.Association AND Countries.Name = RefereesTemp.Country
GO

DROP TABLE RefereesTemp
GO

-- �����
CREATE TABLE EquipmentTemp
(
    Name nvarchar(50)
)
GO

BULK INSERT EquipmentTemp
FROM 'D:\Programming\Projects\SQL\fifa2010\FIFA2010\�����.txt'
WITH (ROWTERMINATOR = '\n')
GO

INSERT INTO Equipment (Name)
SELECT Name
FROM EquipmentTemp
GO

DROP TABLE EquipmentTemp
GO

-- ������� (����� ���� ��������� �������)
CREATE TABLE TrainersTemp
(
    Country nvarchar(50),
    Name nvarchar(50)
)
GO

BULK INSERT TrainersTemp
FROM 'D:\Programming\Projects\SQL\fifa2010\FIFA2010\�������.txt'
WITH (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n')
GO

INSERT INTO Teams (CountryFK, Victories, Loses)
SELECT Countries.Id, 0, 0
FROM TrainersTemp, Countries
WHERE TrainersTemp.Country = Countries.Name
GROUP BY Countries.Id
GO

INSERT INTO Trainers (TeamFK, Name)
SELECT Teams.Id, TrainersTemp.Name
FROM TrainersTemp, Teams, Countries
WHERE Teams.CountryFK = Countries.Id AND Countries.Name = TrainersTemp.Country
GO

DROP TABLE TrainersTemp
GO

-- ����������� ������ ��������
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'FillTeamWithPlayers' AND type = 'P')
    DROP PROCEDURE FillTeamWithPlayers;
GO

CREATE PROCEDURE FillTeamWithPlayers
	@teamId bigint
AS
BEGIN
	DECLARE @i int
	SET @i = 0
	WHILE @i < 23
	BEGIN
		UPDATE Players
		SET TeamFK = @teamId
		WHERE Id = (@teamId - 1) * 23 + @i + 1
		SET @i = @i + 1
	END	
END
GO

DECLARE @tmpTeamId int
SET @tmpTeamId = 1
WHILE @tmpTeamId <= 32
BEGIN
	EXEC FillTeamWithPlayers @tmpTeamId
	SET @tmpTeamId = @tmpTeamId + 1
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GetRandomEquipment' AND type = 'P')
    DROP PROCEDURE GetRandomEquipment;
GO

CREATE PROCEDURE GetRandomEquipment
	@equipment bigint OUTPUT
AS
BEGIN
	SET @equipment = (SELECT TOP(1) Id FROM Equipment ORDER BY NEWID())
END
GO

DECLARE @tmpTeamId int
DECLARE @equipment bigint
SET @tmpTeamId = 1
WHILE @tmpTeamId <= 32
BEGIN
	EXEC GetRandomEquipment @equipment OUTPUT
	UPDATE Teams
	SET EquipmentFK = @equipment
	WHERE Id = @tmpTeamId
	SET @tmpTeamId = @tmpTeamId + 1
END
GO